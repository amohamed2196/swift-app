//
//  QuizeBrain.swift
//  Quizzler-iOS13
//
//  Created by Mero on 2020-04-24.
//  Copyright © 2020 The App Brewery. All rights reserved.
//

import Foundation

struct QuizeBrain {
    let quiz = [
        Question(q: "Which is the largest organ in the human body?", a: ["Heart", "Skin", "Large Intestine"], correctAnswer: "Skin"),
        Question(q: "Five dollars is worth how many nickels?", a: ["25", "50", "100"], correctAnswer: "100"),
        Question(q: "What do the letters in the GMT time zone stand for?", a: ["Global Meridian Time", "Greenwich Mean Time", "General Median Time"], correctAnswer: "Greenwich Mean Time"),
        Question(q: "What is the French word for 'hat'?", a: ["Chapeau", "Écharpe", "Bonnet"], correctAnswer: "Chapeau"),
        Question(q: "In past times, what would a gentleman keep in his fob pocket?", a: ["Notebook", "Handkerchief", "Watch"], correctAnswer: "Watch"),
        Question(q: "How would one say goodbye in Spanish?", a: ["Au Revoir", "Adiós", "Salir"], correctAnswer: "Adiós"),
        Question(q: "Which of these colours is NOT featured in the logo for Google?", a: ["Green", "Orange", "Blue"], correctAnswer: "Orange"),
        Question(q: "What alcoholic drink is made from molasses?", a: ["Rum", "Whisky", "Gin"], correctAnswer: "Rum"),
        Question(q: "What type of animal was Harambe?", a: ["Panda", "Gorilla", "Crocodile"], correctAnswer: "Gorilla"),
        Question(q: "Where is Tasmania located?", a: ["Indonesia", "Australia", "Scotland"], correctAnswer: "Australia")
    ]
    var questionNumber = 0
    var Score = 0
    
    //we use _ so we dont have to specifiy an external name for the method when it is called outside this file
    //the first parmetar is the external name and the second on is the internal name that we can use
    mutating func checkAnswer(_ userAnswer: String) -> Bool
    {
        //here is where we ise the internal
        if userAnswer == quiz[questionNumber].rightAnswer
        {
            //user got it right
            Score += 1
            return true
        }
        else
        {
            //user got it wrong
            return false
        }
    }
    
    func getScore() -> Int{
        return Score
    }
    //Need a way of fetching the answer choices.
    func getAnswers() -> [String] {
        return quiz[questionNumber].answers
    }
    
    func getQuestioinText () -> String
    {
        let getQuestionText = quiz[questionNumber].text
        return getQuestionText
    }
    
    func getProgress () -> Float
    {
        let getProgress = Float ( questionNumber + 1) / Float ( quiz.count )
        return getProgress
    }

    mutating func nextQuestion()
    {
        //loop through the list of quesitons that was created
        if questionNumber + 1 < quiz.count {
            questionNumber += 1
        }
        else
        {
            questionNumber = 0
            Score = 0
        }
    }
    
}
