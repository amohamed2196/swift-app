//
//  Sotry.swift
//  Destini-iOS13
//
//  Created by Angela Yu on 08/08/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import Foundation
struct Story
{
    let t: String
    let C1: String
    let C2: String
    let c1D: Int
    let c2D: Int
    init(title: String, choice1: String, choice1Destination: Int, choice2: String,  choice2Destination: Int )
    {
        t = title
        C1 = choice1
        c1D = choice1Destination
        C2 = choice2
        c2D = choice2Destination
    }
}
