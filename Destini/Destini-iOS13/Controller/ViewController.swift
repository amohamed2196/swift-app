//
//  ViewController.swift
//  Destini-iOS13
//
//  Created by Angela Yu on 08/08/2019.
//  Copyright © 2019 The App Brewery. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var storyLabel: UILabel!
    @IBOutlet weak var choice1Button: UIButton!
    @IBOutlet weak var choice2Button: UIButton!
    
    var storybrain = StoryBrain()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    
    @IBAction func buttonPresed(_ sender: UIButton) {
        storybrain.nextQuestion()
        Timer.scheduledTimer(timeInterval: 0.2, target:self, selector: #selector(updateUI), userInfo:nil, repeats: false)
        updateUI()
    }
    
    @objc func updateUI()
    {
        storyLabel.text = storybrain.getQuestioinText()
        let answerChoices1 = storybrain.getAnswers1()
        choice1Button.setTitle(answerChoices1, for: .normal)
        let answerChoices2 = storybrain.getAnswers2()
        choice2Button.setTitle(answerChoices2, for: .normal)
        
        choice1Button.backgroundColor = UIColor.clear
        choice2Button.backgroundColor = UIColor.clear
    }
    
}

