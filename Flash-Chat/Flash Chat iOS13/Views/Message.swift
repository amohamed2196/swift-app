//
//  Message.swift
//  Flash Chat iOS13
//
//  Created by Mero on 2020-05-12.
//  Copyright © 2020 Angela Yu. All rights reserved.
//

import Foundation

struct Message
{
    let sender: String
    let body: String
}
